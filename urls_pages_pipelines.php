<?php
/**
 * Pipelines utilisees par le plugin URLs Pages Personnalisées
 *
 * @plugin     URLs Pages Personnalisées
 * @copyright  2016
 * @author     tcharlss
 * @licence    GNU/GPL
 * @package    SPIP\Urls_pages_personnalisees\pipelines
 */


/**
 * Modifier les requêtes SQL servant à générer les boucles
 *
 * Boucles URLS : exclure des résultats les URLs des pages en absence du critère {tout}
 *
 * @pipeline pre_boucle
 *
 * @param  object $boucle
 * @return objetc $boucle
**/
function urls_pages_pre_boucle($boucle){

	if ($boucle->type_requete == 'spip_urls') {
		$id_table = $boucle->id_table;
		$page     = $id_table .'.page';
		$id_objet = $id_table .'.id_objet';
		$type     = $id_table .'.type';
		// Restreindre aux URLs sans page
		if (!isset($boucle->modificateur['tout'])) {
				$boucle->where[]= array("'='", "'$page'", "'\"\"'");
		}
	}

	return $boucle;
}
